#!/bin/sh

SCRIPT_LOC=$(realpath "$0")
SCRIPT_PATH=$(dirname "$SCRIPT_LOC")

SRC=/home/$USER/
DST=$(dirname "$SCRIPT_PATH")

cp -riv --update=all --preserve=all "$SRC/.vimrc"  "$SRC/.bashrc" "$SRC/.tmux.conf" "$SRC/.xinitrc" "$DST"
rsync -aiv --exclude=.VimballRecord --exclude=plugged --exclude=plug.vim --exclude=autoload "$SRC/.vim/" "$DST/.vim"
rsync -aiv "$SRC/.config/newsboat/" "$DST/.config/newsboat/"
rsync -aiv "$SRC/.config/nvim/" "$DST/.config/nvim/"
rsync -aiv "$SRC/.config/X11/" "$DST/.config/X11/"
