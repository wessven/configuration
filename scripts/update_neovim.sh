#!/bin/bash

pushd ~/git/neovim/
git fetch && git checkout tags/$(git tag --list "v*" | tail -n1) && make clean && make CMAKE_BUILD_TYPE=RelWithDebInfo && sudo make install
popd
