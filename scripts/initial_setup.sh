#!/bin/bash

$GIT_DIR="$1"

# Ensure a "downloads" directory exists
DOWNLOADS_DIR=${HOME}/Downloads
[[ -d "$DOWNLOADS_DIR" ]] || mkdir "$DOWNLOADS_DIR"

# Ensure a local "bin" directory exists
HOME_BIN_DIR=${HOME}/bin
[[ -d "$HOME_BIN_DIR" ]] || mkdir "$HOME_BIN_DIR"

# Ensure a local "lib" directory exists
HOME_LIB_DIR=${HOME}/lib
[[ -d "$HOME_LIB_DIR" ]] || mkdir "$HOME_LIB_DIR"

pushd "$GIT_DIR"

git clone https://github.com/neovim/neovim.git

# Colour themes for Kitty
git clone https://github.com/dexpota/kitty-themes.git
pushd ${HOME}/.config/kitty/
rm -f theme.conf
ln -s "$GIT_DIR"/kitty-themes/themes/gruvbox_dark.conf
popd

# Stderred
git clone https://github.com/ku1ik/stderred.git
pushd stderred
make #&& sudo make install
cp build/libstderred.so "$HOME_LIB_DIR"
popd

popd

# Load the plug-in manager for Vim
curl -fLo ${HOME}/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Load custom utilities
bash update_delta.sh
bash update_minimap.sh
bash update_ruff.sh
bash update_neovim.sh
