#!/bin/bash

ARCH=$(uname -p)
KERNEL=$(uname -s)
REPO=https://github.com/jesseduffield/lazygit
LATEST_VERSION=$(curl -s ${REPO}/tags.atom | grep title | grep -Po 'v\d+\.\d+\.\d+' | sort -nr | head -n1 | tr -d v)
FILE_NAME=lazygit_${LATEST_VERSION}_${KERNEL}_${ARCH}.tar.gz

pushd ~/Downloads
wget "${REPO}/releases/latest/download/${FILE_NAME}"
tar -xzf "${FILE_NAME}"
sudo mv lazygit /usr/local/bin
rm LICENSE README.md
popd
