#!/bin/bash

DL_DIR=~/Downloads
ARCH=$(dpkg --print-architecture)
REPO=https://github.com/dandavison/delta
LATEST_VERSION=$(curl -s ${REPO}/tags.atom | grep title | grep -Po '\d+\.\d+\.\d+' | sort -nr | head -n1)
#FILE_NAME=git-delta_$(cd ~/git/delta/ && git fetch && git describe --tags $(git rev-list --tags --max-count=1))_amd64.deb
FILE_NAME=git-delta_${LATEST_VERSION}_${ARCH}.deb
TARGET=$DL_DIR/$FILE_NAME

wget "${REPO}/releases/latest/download/$FILE_NAME" -O "$TARGET"
sudo dpkg -i "$TARGET"
