#!/bin/sh

SCRIPT_LOC=$(realpath "$0")
SCRIPT_PATH=$(dirname "$SCRIPT_LOC")

SRC=$(dirname "$SCRIPT_PATH")
DST=/home/$USER/

cp -riv --update=all --preserve=all "$SRC/.vimrc"  "$SRC/.bashrc" "$SRC/.tmux.conf" "$DST"
rsync -aiv "$SRC/.vim/" "$DST/.vim"
rsync -aiv "$SRC/.config/" "$DST/.config/"
