#!/bin/bash

ARCH=$(uname -p)
REPO=https://github.com/astral-sh/ruff
LATEST_VERSION=$(curl -s ${REPO}/tags.atom | grep title | grep -Po 'v\d+\.\d+\.\d+' | sort -nr | head -n1 | tr -d v)
FILE_NAME=ruff-${LATEST_VERSION}-${ARCH}-unknown-linux-musl.tar.gz

pushd ~/Downloads
wget "${REPO}/releases/latest/download/${FILE_NAME}"
tar -xzf "${FILE_NAME}"
sudo mv ruff /usr/local/bin
popd
