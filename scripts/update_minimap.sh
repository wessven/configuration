#!/bin/bash

DL_DIR=~/Downloads
ARCH=$(dpkg --print-architecture)
REPO=https://github.com/wfxr/code-minimap
LATEST_VERSION=$(curl -s ${REPO}/tags.atom | grep title | grep -Po 'v\d+\.\d+\.\d+' | sort -nr | head -n1 | tr -d v)
FILE_NAME=code-minimap_${LATEST_VERSION}_${ARCH}.deb
TARGET=$DL_DIR/$FILE_NAME

wget "${REPO}/releases/latest/download/$FILE_NAME" -O "$TARGET"
sudo dpkg -i "$TARGET"
