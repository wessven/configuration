local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		'git',
		'clone',
		'--filter=blob:none',
		'https://github.com/folke/lazy.nvim.git',
		'--branch=stable',
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)


--[[
	NB: lazy.nvim dependencies the leader key to be set before loading plug-ins.
	Not using a custom leader key, but otherwise this is the place to set the custom leader key.
]]--


local ide_filetypes = {
	'java',
	'lua',
	'python',
}


return require('lazy').setup({

	-- Dependencies
	{ "nvim-lua/plenary.nvim" },

	-- Colour schemes
	{ "Shatur/neovim-ayu" },
	{ "ellisonleao/gruvbox.nvim" },
	{ "whatyouhide/vim-gotham" },

	-- Decorations
	{ 'nvim-tree/nvim-web-devicons', },
	{
		"nvim-lualine/lualine.nvim",
		dependencies = {
			'nvim-tree/nvim-web-devicons',
			opt = true,
		},
		lazy = false,
	},
	{
		'goolord/alpha-nvim',
		dependencies = 'nvim-tree/nvim-web-devicons',
	},
	{ 'chentoast/marks.nvim' },

	-- Workflow
	{ "preservim/nerdtree" },
	{ "nvim-telescope/telescope.nvim" },
	{ "Pocco81/true-zen.nvim" },
	--[['olimorris/persisted.nvim']]--
	{ "tpope/vim-speeddating" },
	{ "vimwiki/vimwiki" },
	{ "akinsho/toggleterm.nvim" },

	-- IDE
	{
		"preservim/tagbar",
		init = function()
			vim.g.tagbar_left = 1
		end,
	},
	{ "chrisbra/changesPlugin" },
	--[[
	{
		'nvim-treesitter/nvim-treesitter',
		build = ':TSUpdate',
	},
	]]--
	{ "sheerun/vim-polyglot" },
	{ 'wfxr/minimap.vim' },
	{
		'puremourning/vimspector',
		ft = ide_filetypes,
	},
	{
		'rstacruz/vim-closer',
	},

	-- IDE: Cmp
	{
		"hrsh7th/nvim-cmp",
		lazy = false,
	}, -- The completion plugin
	{
		"hrsh7th/cmp-buffer",
		lazy = false,
	}, -- buffer completions
	{ "hrsh7th/cmp-path" }, -- path completions
	{
		"hrsh7th/cmp-nvim-lsp",
		lazy = false,
	},
	{
		"hrsh7th/cmp-nvim-lua",
		lazy = false,
	},

	-- IDE: LSP
	{
		"neovim/nvim-lspconfig",
		lazy = false,
	}, -- enable LSP
	{
		"williamboman/mason.nvim",
		lazy = false,
	}, -- simple to language server installer
	{
		"williamboman/mason-lspconfig.nvim",
		lazy = false,
	},
	{ "RRethy/vim-illuminate" },

--	-- Load on a combination of conditions: specific filetypes or commands
--	-- Also build code after load (see the "config" key)
--	{
--	  "dense-analysis/ale",
--	  cmd = "ALEEnable",
--		 config = "vim.cmd[[ALEEnable]]",
--	},

})
