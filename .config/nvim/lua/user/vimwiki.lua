vim.g.vimwiki_list = {{
	path = '~/vimwiki/wiki/',
	path_html = '~/vimwiki/html/',
	auto_export = 0,
}}
vim.g.vimwiki_global_ext = 0
vim.g.vimwiki_list_ignore_newline = 0
