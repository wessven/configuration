local status_ok, lualine = pcall(require, "lualine")
if not status_ok then
	return
end

local hide_in_width = function()
	return vim.fn.winwidth(0) > 80
end

local diagnostics = {
	"diagnostics",
	sources = { "nvim_diagnostic" },
	sections = { "error", "warn" },
	symbols = { error = " ", warn = " " },
	colored = true,
	update_in_insert = false,
	always_visible = true,
}

local diff = {
	"diff",
	colored = false,
	symbols = { added = " ", modified = " ", removed = " " }, -- changes diff symbols
	cond = hide_in_width
}

local filetype = {
	"filetype",
	icons_enabled = false,
	icon = nil,
}

local branch = {
	"branch",
	icons_enabled = true,
	icon = "",
}

local filename = {
	"filename",
	icons_enabled = true,
	icon = "",
}

local location = {
	"location",
	padding = 0,
}

local code_pos = {
	function()
		local kind = vim.api.nvim_call_function("tagbar#currenttagtype", {'%s', ''})
		local kind_symbol

		if kind == 'function' or kind == 'method' then
			kind_symbol = '󰊕 '
		elseif kind == 'class' then
			kind_symbol = '󱘎 '
		elseif kind == 'variable' then
			kind_symbol = '󰫧 '
		elseif kind == 'enum' then
			kind_symbol = ' '
		elseif kind == 'import' then
			kind_symbol = '󱑤 '
		elseif kind == 'module' then
			kind_symbol = ' '
		elseif kind == 'typedef' then
			kind_symbol = ' '
		else
			kind_symbol = ''
		end

		return kind_symbol .. vim.api.nvim_call_function("tagbar#currenttag", {'%s', '', 'fs'})
	end,
	icons_enabled = false,
	icon = "󰅩",
}

local spaces = function()
	--return "spaces: " .. vim.api.nvim_buf_get_option(0, "shiftwidth")
  return ''
end

local selectioncount = {
	"selectioncount",
	icons_enabled = true,
	icon = "󰸱",
}

local searchcount = {
	"searchcount",
	icons_enabled = true,
	icon = "",
}

lualine.setup({
	options = {
		icons_enabled = true,
		theme = "auto",
		-- component_separators = { left = "", right = "" },
		-- section_separators = { left = "", right = "" },
		disabled_filetypes = { "alpha", "dashboard", "NvimTree", "Outline", "packer", "minimap", "tagbar", },
		always_divide_middle = true,
	},
	sections = {
		lualine_a = { "mode", },
		lualine_b = { diagnostics },
		lualine_c = { branch, "filename", code_pos, },
		lualine_x = { diff, spaces, filetype, },
		lualine_y = { searchcount, selectioncount, location },
		lualine_z = { "fileformat", "encoding" },
	},
	inactive_sections = {
		lualine_a = {},
		lualine_b = {},
		lualine_c = { "filename" },
		lualine_x = { "location" },
		lualine_y = {},
		lualine_z = {},
	},
	tabline = {},
	extensions = {},
})
