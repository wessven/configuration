local status_ok, vista = pcall(require, "vista")
if not status_ok then
	return
end

vim.g.vista_sidebar_position = 'vertical topleft'
vim.g.vista_stay_on_open = 0
vim.g.vista_default_executive = 'ctags'
vim.g.vista_executive_for = {
	'python' = 'nvim-lspconfig'
}
