-- After pylsp has been installed using Mason, specific Python tools still need to be installed through Pylsp, e.g.
--     :PylspInstall pyls-flake8 pylsp-mypy
-- If a tool stops working, try reinstalling it.

local venv_path = os.getenv('VIRTUAL_ENV')
local py_exe = nil
if venv_path ~= nil then
  py_exe = venv_path .. "/bin/python3"
else
  py_exe = vim.g.python3_host_prog
end

return {
  settings = {
    pylsp = {
      plugins = {
          -- formatter options
          black = { enabled = false },
          autopep8 = { enabled = false },
          yapf = { enabled = false },
          -- linter options
          pylint = { enabled = false, executable = "pylint" },
          pyflakes = { enabled = false },
          pycodestyle = { enabled = false },
          flake8 = {
            enabled = true,
            ignore = {
              'E115',
              'E131',
              'E225',
              'E226',
			  'E228',
              'E252',
              'E265',
              'E501',
              'W503',
              'W504',
            }
          },
          -- type checker
          pylsp_mypy = {
              enabled = true,
              live_mode = true,
              overrides = {'--python-executable', py_exe, true},
              report_progress = false,
              dmypy = false,
          },
          -- auto-completion options
          --jedi_completion = { fuzzy = true },
          -- import sorting
          pyls_isort = { enabled = false },
      },
    },
  },
}
