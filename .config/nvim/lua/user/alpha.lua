local status_ok, alpha = pcall(require, "alpha")
if not status_ok then
	return
end

local function getGreeting()
	local tableTime = os.date("*t")
	local hour = tableTime.hour
	local greetingsTable = {
		[1] = "  Sleep well",
		[2] = "  Good morning",
		[3] = "  Good afternoon",
		[4] = "  Good evening",
		[5] = "󰖔  Good night",
	}
	local greetingIndex = 0
	if hour == 23 or hour < 7 then
		greetingIndex = 1
	elseif hour < 12 then
		greetingIndex = 2
	elseif hour >= 12 and hour < 18 then
		greetingIndex = 3
	elseif hour >= 18 and hour < 21 then
		greetingIndex = 4
	elseif hour >= 21 then
		greetingIndex = 5
	end
	--local handle = io.popen("whoami")
	local handle = io.popen("getent passwd \"$USER\" | cut -d':' -f5 | cut -d' ' -f1")
	local whoami = handle:read("*a")
	handle:close()
	return greetingsTable[greetingIndex] .. ", " .. whoami
end

local dashboard = require("alpha.themes.dashboard")
local greeting = getGreeting():gsub("^%s*(.-)%s*$", "%1")
local greeting_w = greeting:len()
if vim.api.nvim_win_get_width(vim.api.nvim_get_current_win()) > 120 then
	local w = 110
	local padding = (w - greeting_w) / 2
	dashboard.section.header.val = {
		[[                                                                                ████                          ]],
		[[                                                                               █░░░░█                         ]],
		[[                                                                                ████                          ]],
		[[                                                                                                              ]],
		[[████  ████████        ████████████       ███████████ ███████           ██████████████    ███████    ███████   ]],
		[[█░░░██░░░░░░░░██    ██░░░░░░░░░░░░██   ██░░░░░░░░░░░███░░░░░█         █░░░░░█ █░░░░░█  ██░░░░░░░█  █░░░░░░░██ ]],
		[[█░░░░░░░░░░░░░░██  █░░░░░░█████░░░░░███░░░░░░░░░░░░░░░██░░░░░█       █░░░░░█   █░░░░█ █░░░░░░░░░░██░░░░░░░░░░█]],
		[[██░░░░░░░░░░░░░░░██░░░░░░█     █░░░░░██░░░░░█████░░░░░█ █░░░░░█     █░░░░░█    █░░░░█ █░░░░░░░░░░░░░░░░░░░░░░█]],
		[[  █░░░░░████░░░░░██░░░░░░░█████░░░░░░██░░░░█     █░░░░█  █░░░░░█   █░░░░░█     █░░░░█ █░░░░░███░░░░░░███░░░░░█]],
		[[  █░░░░█    █░░░░██░░░░░░░░░░░░░░░░░█ █░░░░█     █░░░░█   █░░░░░█ █░░░░░█      █░░░░█ █░░░░█   █░░░░█   █░░░░█]],
		[[  █░░░░█    █░░░░██░░░░░░███████████  █░░░░█     █░░░░█    █░░░░░█░░░░░█       █░░░░█ █░░░░█   █░░░░█   █░░░░█]],
		[[  █░░░░█    █░░░░██░░░░░░░█           █░░░░█     █░░░░█     █░░░░░░░░░█        █░░░░█ █░░░░█   █░░░░█   █░░░░█]],
		[[  █░░░░█    █░░░░██░░░░░░░░█          █░░░░░█████░░░░░█      █░░░░░░░█        █░░░░░░██░░░░█   █░░░░█   █░░░░█]],
		[[  █░░░░█    █░░░░█ █░░░░░░░░████████  █░░░░░░░░░░░░░░░█       █░░░░░█         █░░░░░░██░░░░█   █░░░░█   █░░░░█]],
		[[  █░░░░█    █░░░░█  ██░░░░░░░░░░░░░█   ██░░░░░░░░░░░██         █░░░█          █░░░░░░██░░░░█   █░░░░█   █░░░░█]],
		[[  ██████    ██████    ██████████████     ███████████            ███           ██████████████   ██████   ██████]],
		[[]],
		[[]],
		[[]],
		string.rep(" ", padding) .. greeting,
	}
else
	local w = 50
	local padding = (w - greeting_w) / 2
	dashboard.section.header.val = {
		[[███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗]],
		[[████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║]],
		[[██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║]],
		[[██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║]],
		[[██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║]],
		[[╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝]],
		[[]],
		string.rep(" ", padding) .. greeting,
	}
end
dashboard.section.buttons.val = {
	--dashboard.button("s", "  Restore previous session", ":SessionLoadLast <CR>"),
	dashboard.button("f", "󰈞  Find file", ":Telescope find_files <CR>"),
	dashboard.button("e", "  New file", ":ene <BAR> startinsert <CR>"),
	dashboard.button("p", "  Find project", ":Telescope projects <CR>"),
	dashboard.button("r", "󰄉  Recently used files", ":Telescope oldfiles <CR>"),
	dashboard.button("t", "󰊄  Find text", ":Telescope live_grep <CR>"),
	dashboard.button("u", "  Check for plugin updates", ":Lazy check<CR>"),
	dashboard.button("c", "  Configuration", ":e $MYVIMRC <CR>"),
	dashboard.button("q", "󰿅  Quit Neovim", "ZQ"),
}

local function fortune()
-- NOTE: requires the fortune-mod package to work
	local handle = io.popen("fortune")
	local fortune = handle:read("*a")
	handle:close()
	return fortune
end

local function footer()
	local plugins_count = vim.fn.len(vim.fn.globpath("~/.local/share/nvim/lazy", "*", 0, 1))
	local datetime = os.date("  %Y-%m-%d   %H:%M")
	local version = vim.version()
	local nvim_version_info = "   v" .. version.major .. "." .. version.minor .. "." .. version.patch

	--[[
	local handle = io.popen('curl -m 2 -sL wttr.in/Cape%20Town?format="%l:+%c+%t+%w"')
	local weather = handle:read("*a")
	handle:close()
	if weather:len() == 0 then
		weather = "轢"
	end

	return datetime .. "  󰏈 " .. weather .. "   Plugins " .. plugins_count .. nvim_version_info
	]]--
	return datetime .. "   Plugins " .. plugins_count .. nvim_version_info
end

dashboard.section.footer.val = footer()

dashboard.section.footer.opts.hl = "Type"
dashboard.section.header.opts.hl = "Include"
dashboard.section.buttons.opts.hl = "Keyword"

dashboard.opts.opts.noautocmd = true
-- vim.cmd([[autocmd User AlphaReady echo 'ready']])
alpha.setup(dashboard.opts)
