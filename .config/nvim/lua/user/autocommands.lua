vim.cmd [[
	augroup _general_settings
		autocmd!
		autocmd FileType qf,help,man,lspinfo nnoremap <silent> <buffer> q :close<CR>
		autocmd FileType help setlocal nospell
		"autocmd TextYankPost * silent!lua require('vim.highlight').on_yank({higroup = 'Visual', timeout = 200})
		autocmd BufWinEnter * :set formatoptions-=cro
		autocmd FileType css setlocal list nu smartindent
		autocmd FileType html,html5 setlocal formatoptions+=tl
		autocmd FileType html,html5,css setlocal noexpandtab tabstop=2
		autocmd FileType html,html5,xml iabbrev </ </<C-X><C-O>
		autocmd FileType sql setlocal nu
		autocmd FileType make setlocal noexpandtab shiftwidth=8
		autocmd FileType qf set nobuflisted
		autocmd FileType asciidoc setlocal list
		autocmd FileType c,cpp,groovy,java,javascript,julia,perl,python :execute "TagbarOpen"
		autocmd FileType perl,python setlocal list nu smartindent
		autocmd FileType python setlocal foldmethod=indent expandtab ts=4 foldignore=# foldlevel=99
		autocmd FileType javascript setlocal cindent nu
		autocmd FileType java setlocal foldmethod=syntax foldenable foldlevel=20 foldnestmax=2 nu nolost noexpandtab
		autocmd BufNewFile,BufRead *.jad setlocal syntax=java nu foldenable foldmethod=syntax foldleve=20 foldnestmax=2
		autocmd BufNewFile,BufRead *.txt,*.html,*.htm,README*,INSTALL*,LICENSE* setlocal spell
		autocmd BufNewFile,BufRead *.yml,*.yaml setlocal nu expandtab foldmethod=indent foldlevel=99
		autocmd BufWritePre *.{c,cpp,css,go,java,js,lua,py,sql,tsql,pas} %s/\s\+$//e
	augroup end

	augroup _diff
		autocmd BufWritePost * if &diff == 1 | diffupdate | endif
		autocmd VimResized * if &diff == 1 | wincmd = | endif
	augroup end

	augroup _git
		autocmd!
		autocmd FileType gitcommit setlocal wrap
		autocmd FileType gitcommit setlocal spell
	augroup end

	augroup _markdown
		autocmd!
		autocmd FileType markdown setlocal wrap
		autocmd FileType markdown setlocal spell
	augroup end

	augroup _auto_resize
		autocmd!
		autocmd VimResized * tabdo wincmd =
	augroup end

	augroup _alpha
		autocmd!
		autocmd User AlphaReady set showtabline=0 | autocmd BufUnload <buffer> set showtabline=2
	augroup end
]]

-- Autoformat
-- augroup _lsp
--   autocmd!
--   autocmd BufWritePre * lua vim.lsp.buf.formatting()
-- augroup end

-- https://vim.fandom.com/wiki/Diff_current_buffer_and_the_original_file
vim.cmd([[
	function! s:DiffWithSaved()
		let filetype=&ft
		diffthis
		vnew | read # | normal! 1Gdd
		diffthis
		exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
	endfunction
	com! DiffSaved call s:DiffWithSaved()

	function! s:CloseUnnecessaryStuff()
		MinimapClose
		TagbarClose
		NERDTreeClose
	endfunction
	com! FluffOff call s:CloseUnnecessaryStuff()

	function! s:DiffWindows()
		let have_diff = getwinvar(winnr('#'), '&diff')
		if have_diff
			diffoff
		else
			:windo diffthis
		endif
	endfunction
	com! ToggleDiff call s:DiffWindows()
]])
