local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

--Remap space as leader key
--keymap("", "<Space>", "<Nop>", opts)
--vim.g.mapleader = " "
--vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- Normal --
--keymap("n", "<Leader>n", ":let g:vista_sidebar_position = 'vertical topleft'<CR>:let g:vista_stay_on_open=0<CR>:Vista<CR>", opts)

-- Switch off highlighted words
keymap('n', '<Leader>H', '<cmd>noh<CR>', opts)
-- Diff modified buffer with (saved) file contents - https://vim.fandom.com/wiki/Diff_current_buffer_and_the_original_file
keymap('n', '<Leader>D', '<cmd>DiffSaved<CR><cmd>TagbarClose<CR><cmd>MinimapClose<CR>', opts)
-- Nuke buffer
keymap('n', '<Leader>N', 'bp|bd #<CR>', opts)

--------------------------------------------------------
-- FUNCTION KEYS
--------------------------------------------------------

-- F1: Help
-- F2: Defined in lsp/handlers.lua
-- F3: Defined in lsp/handlers.lua
-- F4: Close unnecessary windows
keymap('n', '<F4>', '<cmd>FluffOff<CR>', opts)

-- F5: Defined in vimspector.lua
-- F6: File browser; can be overwritten by NERDTree config
keymap('n', '<F6>', '<cmd>Hexplore<CR>', term_opts)

-- F7: Spell check
keymap('n', '<F7>', '<cmd>set spell!<CR>', opts)

-- F8: Code navigation
vim.cmd [[
	if exists(':TagbarOpen')
		:nnoremap <F8> :TagbarToggle<CR>
	else
		if exists(':TlistOpen')
			:nnoremap <F8> :TlistToggle<CR>
		endif
	endif
]]

-- F9: TODO Save session

-- F10: Diff files
keymap('n', '<F10>', '<cmd>ToggleDiff<CR>', opts)

-- F11: "Full screen" / focus mode
vim.cmd [[
	if exists(':Goyo')
		:nnoremap <F11> :Goyo 82<CR>
	endif
]]

-- F12: Left empty (mapped by window manager)

-- Writing
keymap("i", "<Leader>q", '<C-k>6"<C-k>"9<Esc>i', opts)

-- Better window navigation
keymap("n", "<C-Left>", "<C-w>h", opts)
keymap("n", "<C-Down>", "<C-w>j", opts)
keymap("n", "<C-Up>", "<C-w>k", opts)
keymap("n", "<C-Right>", "<C-w>l", opts)

-- Resize with arrows
keymap("n", "<C-h>", "<cmd>vertical resize -2<CR>", opts)
keymap("n", "<C-j>", "<cmd>resize +2<CR>", opts)
keymap("n", "<C-k>", "<cmd>resize -2<CR>", opts)
keymap("n", "<C-l>", "<cmd>vertical resize +2<CR>", opts)

-- Navigate buffers
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- Move text up and down
keymap("n", "<A-j>", "<Esc>:m .+1<CR>==gi", opts)
keymap("n", "<A-k>", "<Esc>:m .-2<CR>==gi", opts)

-- Visual --
-- Stay in indent mode
--keymap("v", "<", "<gv", opts)
--keymap("v", ">", ">gv", opts)

-- Move text up and down
keymap("v", "<A-j>", ":m .+1<CR>==", opts)
keymap("v", "<A-k>", ":m .-2<CR>==", opts)
keymap("v", "p", '"_dP', opts)

-- Visual Block --
-- Move text up and down
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)

-- Terminal --
-- Better terminal navigation
-- keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
-- keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
-- keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
-- keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

