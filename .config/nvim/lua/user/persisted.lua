local status_ok, persisted = pcall(require, "persisted")
if not status_ok then
	return
end

local keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

persisted.setup({
	autosave = false,
})
keymap("n", "<F9>", ":SessionSave<CR>", opts)
