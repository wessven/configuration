require('user.plugins')
require('user.colourscheme')
require('user.changes')
require('user.cmp')
require('user.lsp')
require('user.treesitter')
require('user.telescope')
require('user.minimap')
require('user.lualine')
--require('user.persisted')
require('user.alpha')
require('user.autocommands')
require('user.keymaps')
require('user.nerdtree')
require('user.vimspector')
require('user.marks')
require('user.tagbar')
require('user.options')
