source $HOME/.vim/options.vim
source $HOME/.vim/plugins.vim
source $HOME/.vim/display.vim
source $HOME/.vim/keymaps.vim
source $HOME/.vim/autocommands.vim
