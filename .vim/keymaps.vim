" --------------------
" Automatic pairs
" --------------------
inoremap <leader>" “”<left>
inoremap <leader>' ‘’<left>
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
" Don't duplicate when muscle memory takes over
inoremap "" ""
inoremap '' ''
inoremap () ()
inoremap [] []
inoremap {} {}

" --------------------
" Function keys
" --------------------
inoremap <F3> <C-k>6"<C-k>"9<Esc>i
nnoremap <F7> :set spell!<Return>
if v:version >= 800
	:nnoremap <F9> :term<Return>
endif
nnoremap <F10> bp\|bd #<Return>

" --------------------
" Autocomplete
" --------------------
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" --------------------
" Override mswin.vim
" --------------------
if (has('win32') || has('win64') || has('win16'))
	" Avoid mswin.vim making Ctrl-v act as paste
	noremap <C-V> <C-V>
endif
