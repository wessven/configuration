let s:goyo_width = 81

let b:writing_filetypes = join([
	\ 'asciidoc',
	\ 'markdown',
	\ 'rst',
	\ 'txt',
	\], ',')
let b:low_level_languages = join([
	\ 'asm',
	\], ',')
let b:query_languages = join([
	\ 'sql',
	\], ',')
let b:styling_languages = join([
	\ 'css',
	\ 'sass',
	\ 'scss',
	\], ',')
let b:markup_languages = join([
	\ 'html',
	\ 'xhtml',
	\ 'xml',
	\], ',')
let b:webdev_languages = join([
	\ 'angular',
	\ 'react',
	\ 'vue',
	\], ',')
let b:coding_languages = join([
	\ 'c',
	\ 'cpp',
	\ 'go',
	\ 'groovy',
	\ 'haskell',
	\ 'java',
	\ 'javascript',
	\ 'julia',
	\ 'kotlin',
	\ 'lua',
	\ 'pascal',
	\ 'php',
	\ 'powershell',
	\ 'python',
	\ 'r',
	\ 'ruby',
	\ 'rust',
	\ 'scala',
	\ 'typescript',
	\ 'vim',
	\ 'yaml',
	\], ',')
let b:coding_filetypes = b:low_level_languages . ',' . b:query_languages . ',' . b:styling_languages . ',' . b:markup_languages . ',' . b:webdev_languages . ',' . b:coding_languages

" --------------------
" vim-plug
" --------------------
call plug#begin()
	" Make sure you use single quotes
	" Shorthand notation for GitHub; translates to https://github.com/junegunn/vim-easy-align
	" Any valid git URL is allowed

	"Plug 'rhysd/vim-healthcheck'
	"Plug 'dstein64/vim-startuptime'

	" --- Decoration
	Plug 'vim-airline/vim-airline'
	Plug 'vim-scripts/ShowMarks'
	Plug 'wfxr/minimap.vim'

	" --- Colour schemes
	Plug 'ayu-theme/ayu-vim'
	Plug 'morhetz/gruvbox'
	Plug 'whatyouhide/vim-gotham'

	" --- IDE
	Plug 'preservim/tagbar'
	Plug 'andymass/vim-matchup'
	Plug 'sheerun/vim-polyglot'	" Already lazy
	Plug 'alvan/vim-closetag', { 'for': b:markup_languages . ',php' }
	Plug 'RRethy/vim-hexokinase', { 'do': 'make hexokinase', 'for': b:styling_languages }
	Plug 'dense-analysis/ale', { 'for': b:coding_filetypes }
	Plug 'prabirshrestha/vim-lsp', { 'for': b:coding_filetypes }
	Plug 'rhysd/vim-lsp-ale', { 'for': b:coding_filetypes }
	Plug 'mattn/vim-lsp-settings', { 'for': b:coding_filetypes }
	Plug 'lifepillar/vim-mucomplete'
	"Plug 'SirVer/ultisnips'
	"Plug 'honza/vim-snippets'
	Plug 'itchyny/vim-gitbranch'

	" --- Writing
	Plug 'junegunn/goyo.vim', { 'branch': 'master', 'for': b:writing_filetypes }
	Plug 'junegunn/limelight.vim', { 'for': b:writing_filetypes }
	Plug 'preservim/vim-pencil', { 'for': b:writing_filetypes }

	" --- Productivity
	Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' }	" On-demand loading: loaded when the specified command is executed
	Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
	Plug 'junegunn/fzf.vim'
	Plug 'tpope/vim-speeddating'
	Plug 'mhinz/vim-signify'
	Plug 'vimwiki/vimwiki'

	" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
	"Plug 'fatih/vim-go', { 'tag': '*' }

call plug#end()

" -----------------------
" Plugin global variables
" -----------------------
let g:airline_powerline_fonts = 1
let g:airline_exclude_filetypes = ['__Tagbar__', '-MINIMAP-']
let g:airline#extensions#branch#custom_head = 'gitbranch#name'
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#enabled = 1
let g:go_version_warning = 0
let g:minimap_highlight_search = 1
let g:minimap_git_colors = 1
let g:mucomplete#enable_auto_at_startup = 1
let g:mucomplete#completion_delay = 50
let g:mucomplete#tab_when_no_results = 1
let g:netrw_altv = 1
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_liststyle = 3
let g:netrw_winsize = 25
let g:showmarks_include = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
let g:showmarks_enable = 1
let g:tagbar_left = 1
let g:Tlist_Exit_OnlyWindow = 1
let g:vimwiki_list = [{'path' : '~/vimwiki/wiki/', 'path_html' : '~/vimwiki/html', 'auto_export' : 1, 'html_header' : '~/vimwiki/header.tpl', 'html_footer' : '~/vimwiki/footer.tpl'}]
let g:vimwiki_list_ignore_newline = 0

" --------------------
" Plugins setup
" --------------------
function OpenCodeNavigation()

	if exists(':TagbarOpen')
		:TagbarOpen
		let g:airline#extensions#tagbar#flags = 'f'
	else
		if exists(':TlistOpen')
			:TlistOpen
		endif
	endif

	if exists(':Minimap')
		:Minimap
	endif

endfunction

function! s:SetPluginOptions()

	" --------------------
	" File browser
	" --------------------
	if exists(':FZF')
		:nnoremap <F6> :FZF<Return>
	elseif exists(':NERDTreeToggle')
		:nnoremap <F6> :NERDTreeToggle<Return>
	else
		" Default to netrw, which is bundled with Vim
		:nnoremap <F6> :Hexplore<Return>
	endif

	" --------------------
	" Code navigation
	" --------------------
	if exists(':TagbarToggle')
		:nnoremap <F8> :TagbarToggle<Return>
	else
		if exists(':TlistToggle')
			:nnoremap <F8> :TlistToggle<Return>
		endif
	endif

	" ----------------------------
	" ``Full screen'' / Focus mode
	" ----------------------------
	if exists(':Goyo')
		if exists(':Limelight')
			autocmd! User GoyoEnter Limelight
			autocmd! User GoyoLeave Limelight!
			if exists(':Pencil')
				:nnoremap <F11> :Goyo s:goyo_width<Return>:Limelight<Return>:Pencil<Return>
				let g:airline_section_x = '%{PencilMode()}'
				let g:pencil#conceallevel = 0
				let g:pencil#joinspaces = 1
				let g:pencil#textwidth = 79
			else
				:nnoremap <F11> :Goyo s:goyo_width<Return>:Limelight<Return>
			endif
		else
			:nnoremap <F11> :Goyo s:goyo_width<Return>
		endif
	endif

	function! CustomOpts() abort
		let [l:info, l:loc] = ale#util#FindItemAtCursor(bufnr(''))
		return {'title': ' ALE: ' . (l:loc.linter_name) . ' '}
	endfunction

	"if exists(':ALEInfo')
	let g:airline#extensions#ale#enabled = 1
	let g:ale_command_wrapper = 'nice -n5'
	let g:ale_completion_delay = 100
	let g:ale_completion_enabled = 0
	let g:ale_completion_max_suggestions = 5
	let g:ale_cursor_detail = 1
	let g:ale_detail_to_floating_preview = 1
	let g:ale_disable_lsp = 1
	let g:ale_floating_preview = 1
	let g:ale_floating_window_border = ['│', '─', '╭', '╮', '╯', '╰', '│', '─']
	let g:ale_hover_to_floating_preview = 1
	let g:ale_popup_menu_enabled = 1
	let g:ale_set_balloons = 1
	let g:ale_sign_error = '🔴'		" '🛑'
	let g:ale_sign_warning = '🟡'	" '⚠️'
	if &background ==# 'dark'
		let g:ale_sign_style_error = '⚪'
		let g:ale_sign_style_warning = '⚫'
	else
		let g:ale_sign_style_error = '⚫'
		let g:ale_sign_style_warning = '⚪'
	endif
	let g:ale_sign_info = '💡'		" '🛈 '
	let g:ale_virtualtext_cursor = 'all'
	let g:ale_virtualtext_delay = 10
	let g:ale_virtualtext_prefix = '%comment% %linter%[%code%]: '
	let g:ale_echo_msg_format = '%code: %%s'
	let g:ale_windows_node_executable_path = $HOME . '/Applications/node-v12.14.1-win-x64/node.exe'
	let g:ale_floating_preview_popup_opts = 'g:CustomOpts'

	let g:ale_linters = {"python": ["mypy", "flake8",]}
	let g:ale_lint_on_enter = 1
	let g:ale_lint_on_save = 1
	let g:ale_lint_on_insert_leave = 1
	let g:ale_lint_on_text_changed = 1
	let g:ale_lint_delay = 200
	let g:ale_linters_explicit = 1
	let g:ale_warn_about_trailing_blank_lines = 1
	let g:ale_warn_about_trailing_whitespace = 1
	let g:ale_python_mypy_executable = $HOME . '/AppData/Roaming/Python/Python312/Scripts/mypy.exe'
	let g:ale_python_flake8_executable = $HOME . '/AppData/Roaming/Python/Python312/Scripts/flake8.exe'
	let g:ale_python_ruff_executable = $HOME . '/AppData/Roaming/Python/Python312/Scripts/ruff.exe'

	let g:ale_fixers = {"python": []}
	let g:ale_fix_on_save = 0
	"let g:ale_python_ruff_options = '--ignore=E501,E402 \ --target-version=py310 \ --quiet'
	"set omnifunc=ale#completion#OmniFunc
	"endif
	
	let g:matchup_matchparen_offscreen = {}

	if exists(':LspStatus')

"		au User lsp_setup call lsp#register_server({
"			\ 'name': 'ruff-lsp',
"			\ 'cmd': {server_info->[$HOME . '/.local/share/vim-lsp-settings/servers/ruff-lsp/ruff-lsp']},
"			\ 'allowlist': ['python'],
"			\ },{
"			\ 'name': 'sumneko-lua-language-server',
"			\ 'cmd': {server_info->[$HOME . '/.local/share/vim-lsp-settings/servers/sumneko-lua-language-server']},
"			\ 'allowlist': ['lua'],
"			\ })

		function! s:on_lsp_buffer_enabled() abort
			setlocal omnifunc=lsp#complete
			setlocal signcolumn=yes
			if exists ('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
			nmap <buffer> gd <plug>(lsp-definition)
			nmap <buffer> gD <plug>(lsp-declaration)
			nmap <buffer> gs <plug>(lsp-document-symbol-search)
			nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
			nmap <buffer> gr <plug>(lsp-references)
			nmap <buffer> gi <plug>(lsp-implementation)
			nmap <buffer> <leader>lT <plug>(lsp-type-definition)
			nmap <buffer> [g <plug>(lsp-previous-diagnostic)
			nmap <buffer> ]g <plug>(lsp-next-diagnostic)
			nmap <buffer> K <plug>(lsp-hover)
			nmap <buffer> <c-k> <plug>(lsp-signature_help)
			"nmap <buffer> <leader>rn <plug>(lsp-rename)
			nmap <buffer> <F2> <plug>(lsp-rename)
			nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
			nnoremap <buffer> <expr><c-d> lsp#scroll(-4)

			let g:lsp_completion_documentation_enabled = 1
			let g:lsp_completion_documentation_delay = 80
			let g:lsp_completion_resolve_timeout = 0
`
			" Defer diagnostics (linting) to ALE
			let g:lsp_diagnostics_enabled = 0
			let g:lsp_diagnostics_echo_cursor = 0
			let g:lsp_diagnostics_float_cursor = 0
			let g:lsp_diagnostics_highlights_enabled = 0
			let g:lsp_diagnostics_signs_enabled = 0
			let g:lsp_diagnostics_virtual_text_enabled = 0

			let g:lsp_inlay_hints_enabled = 1
			let g:lsp_document_code_action_signs_enabled = 1
			let g:lsp_document_code_action_signs_delay = 200
			let g:lsp_document_highlight_enabled = 1
			let g:lsp_document_highlight_delay = 200
			let g:lsp_format_sync_timeout = 1000
			"autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')

			" Performance
			let g:lsp_use_native_client = 1
			let g:lsp_semantic_enabled = 0
			let g:lsp_text_document_did_save_delay = -1
			let g:lsp_format_sync_timeout = 1000
			let g:lsp_max_buffer_size = 10000000

			" LSP to manage folding
			set foldmethod=expr
			set foldexpr=lsp#ui#vim#folding#foldexpr()
			set foldtext=lsp#ui#vim#folding#foldtext()

			let g:lsp_async_completion = 1

		endfunction

		"augroup lsp_install
		"	au!
		" Call s:on_lsp_buffer_enabled only for languages that has the server registered
		autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
		"augroup END

		let g:lsp_settings = {
			\	'ruff-lsp': {
			\		'cmd': [get(g:, 'lsp_settings_servers_dir', expand('~/.local/share/vim-lsp-settings/servers')) . '/ruff-lsp/ruff-lsp'],
			\		'allowlist': ['python'],
			\		'disabled': 1,
			\	},
			\	'sumneko-lua-language-server': {
			\		'cmd': [get(g:, 'lsp_settings_servers_dir', expand('~/.local/share/vim-lsp-settings/servers')) . '/sumneko-lua-language-server/sumneko-lua-language-server'],
			\		'allowlist': ['lua'],
			\		'disabled': 0,
			\	},
			\   'pylsp': {
			\		'cmd': 'C:/Users/wventer/AppData/Local/vim-lsp-settings/servers/pylsp/pylsp.cmd',
			\		'allowlist': ['python'],
			\       'disabled': 0,
			\   },
			\   'pylsp-all': {
			\	  'disabled': 1,
			\     'workspace_config': {
			\       'pylsp': {
			\         'configurationSources': ['flake8'],
			\         'plugins': {
			\           'flake8': {
			\             'enabled': v:true,
			\           },
			\           'black': {
			\             'enabled': v:false
			\           },
			\           'autopep8': {
			\             'enabled': v:false
			\           },
			\           'yapf': {
			\             'enabled': v:false
			\           },
			\           'pylint': {
			\             'enabled': v:false,
			\             'executable': 'pylint'
			\           },
			\           'pyflakes': {
			\             'enabled': v:false
			\           },
			\           'pycodestyle': {
			\             'enabled': v:false
			\           },
			\           'pylsp_mypy': {
			\             'enabled': v:false
			\           },
			\           'jedi_completion': {
			\             'fuzzy': v:true
			\           },
			\           'pyls_isort': {
			\             'enabled': v:false
			\           },
			\         },
			\       }
			\     },
			\   },
			\	'pyls': {
			\		'disabled': 1,
			\		'workspace_config': {
			\			'pyls': {
			\				 'plugins': {
			\					'mypy': {'enabled': v:false},
			\					'flake8': {'enabled': v:false},
			\					'pydocstyle': {'enabled': v:false},
			\					'pycodestyle': {'enabled': v:false},
			\					'pyflakes': {'enabled': v:false},
			\					'jedi': {'enabled': v:true},
			\					'pylint': {'enabled': v:false}
			\				}
			\			}
			\	}
			\ }
		\}

	endif

	" --------------------
	" ShowMarks
	" --------------------
	if exists(':ShowMarksToggle')

		" For marks a-z
		highlight ShowMarksHLl gui=bold guibg=LightBlue guifg=Blue
		" For marks A-Z
		highlight ShowMarksHLu gui=bold guibg=LightRed guifg=DarkRed
		" For all other marks
		highlight ShowMarksHLo gui=bold guibg=LightYellow guifg=DarkYellow
		" For multiple marks on the same line.
		highlight ShowMarksHLm gui=bold guibg=LightGreen guifg=DarkGreen

	endif

endfunction

au VimEnter * call s:SetPluginOptions()
