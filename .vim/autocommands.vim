augroup grp_personal_settings
	autocmd!

	autocmd FileType asciidoc setlocal list
	autocmd FileType help setlocal nospell
	autocmd BufNewFile,BufRead *.adoc call s:PopulateAdoc()

	" --------------------
	" Development
	" --------------------
	autocmd FileType c setlocal formatoptions+=ro
	autocmd FileType c,cpp,slang setlocal cindent list nu
	autocmd Filetype c,cpp,java,pas,python call OpenCodeNavigation()
	autocmd FileType css setlocal list nu smartindent
	autocmd FileType go setlocal list nu
	autocmd FileType html,html5 setlocal formatoptions+=tl
	autocmd FileType html,html5,css setlocal noexpandtab tabstop=2
	"autocmd FileType html,html5,xml iabbrev </ </<C-X><C-O>
	autocmd FileType java setlocal foldmethod=syntax foldenable foldlevel=20 foldnestmax=2 nu list ofu=syntaxcomplete:Complete
	autocmd FileType javascript setlocal cindent nu
	autocmd FileType make setlocal noexpandtab shiftwidth=8
	autocmd FileType pas setlocal nu
	autocmd FileType perl,python setlocal list nu smartindent
	autocmd FileType php inoremap <buffer> <?php <?php<space><space>?><left><left><left>
	autocmd FileType python setlocal foldmethod=indent expandtab ts=4
	"autocmd FileType python setlocal foldmethod=indent tw=78 colorcolumn=+1 expandtab ts=4
	"autocmd FileType python highlight ColorColumn ctermbg=grey guibg=darkgrey
	autocmd FileType sql setlocal nu
	autocmd FileType vim inoremap <buffer> " "
	autocmd BufNewFile,BufRead *.jad set filetype=java
	autocmd BufWritePre *.{c,cpp,css,go,java,js,py,sql,tsql,pas} %s/\s\+$//e
	autocmd BufWritePost .tmux.conf,.tmux.local.conf execute ':!tmux source-file %'
	autocmd BufNewFile,BufRead *.txt,*.html,*.htm,README*,INSTALL*,LICENSE* setlocal spell
	autocmd BufNewfile,BufRead *.htm,*.html call s:PopulateHTML()

	" --------------------
	" Vimdiff
	" --------------------
	autocmd BufWritePost * if &diff == 1 | diffupdate | endif
	autocmd VimResized * if &diff == 1 | wincmd = | endif

	" --------------------
	" Vim todo.txt
	" --------------------
	autocmd BufRead done.txt setlocal autoread
	autocmd BufRead todo.txt,done.txt setlocal nospell
	autocmd FocusLost,WinLeave todo.txt,done.txt :silent! w
	autocmd VimResized todo.txt,done.txt wincmd =
	" Automatically highlight dates
	autocmd BufRead,FocusGained todo.txt call s:HighlightDates()

augroup END

function! s:PopulateHTML()
	if !&readonly && line('$') == 1 && getline(1) == ''
		r~/.vim/templates/empty.html
		call cursor(1,1)
		del 1
		call cursor(11,2)
	endif
endfunction

function! s:PopulateAdoc()
	if !&readonly && line('$') == 1 && getline(1) == ''
		r~/.vim/templates/empty.adoc
		call cursor(1,1)
		del 1
		call setline(3, ":revdate: " . strftime("%Y-%m-%d %H:%M:%S %z"))
		call cursor(1,2)
	endif
endfunction

" https://vim.fandom.com/wiki/Diff_current_buffer_and_the_original_file
function! s:DiffWithSaved()
	let filetype=&ft
	diffthis
	vnew | read # | normal! 1Gdd
	diffthis
	exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()

function! s:HighlightDates()
	let l:currentdate = strftime("%Y-%m-%d")
	let l:syntaxtoday = "syn match TodoToday '" . l:currentdate . "' containedin=TodoDate"
	exec l:syntaxtoday
	hi link TodoToday Todo
	let l:tomorrowdate = strftime("%Y-%m-%d", localtime() + 60*60*24)
	let l:syntaxtomorrow = "syn match TodoTomorrow '" . l:tomorrowdate . "' containedin=TodoDate"
	exec l:syntaxtomorrow
	hi link TodoTomorrow MatchParen
endfunction
