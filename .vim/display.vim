set guifont=DejaVuSansM\ Nerd\ Font\ Mono,DejaVu\ Sans\ Mono\ for\ Powerline,DejaVu\ Sans\ Mono,monospace:10

if &term == 'screen' || &term == 'tmux'
	set t_Co=256
endif

if &term =~ '256color' || has('gui_running') || has('nvim') || has('vcon') || &t_Co > 2

	set termguicolors

	colorscheme desert
	silent! colorscheme gruvbox

	if has('syntax')
		syntax on
		set hlsearch
	endif

	if (has('win32') || has('win64') || has('win16')) && !has('nvim')
		set rop=type:directx,gamma:1.0,contrast:0.5,level:1,geom:1,renmode:4,taamode:1
	endif

"else
"	let g:airline_left_sep = '»'
"	let g:airline_right_sep = '«'
endif

if &encoding == "utf-8"
	set listchars=tab:▸\ ,eol:↵,nbsp:⎵,trail:·,precedes:❮,extends:❯
	let &showbreak = '  ↪  '
else
	set listchars=tab:>\ ,eol:<,nbsp:~,trail:.,precedes:{,extends:}
	let &showbreak = ' ----> '
endif
