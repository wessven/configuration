# .bashrc

# User specific aliases and functions

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Some handy aliases
alias cd..='cd ..'
alias ll='ls -l'
alias vi=vim

# My favourite CLI editor...
export EDITOR=vi
export VISUAL=vim

# Don't forget: "colour" sequences must be escaped by \[ and \]
export PS1='\[\033[00;01m\][${USER}@${HOSTNAME} \W]\$\[\033[0m\] '

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
    ;;
*)
    ;;
esac

# Ignore duplicates in my BASH history, please.
export HISTFILESIZE=5242880
export HISTSIZE=1000000
export HISTTIMEFORMAT="[%F %T] "
export HISTCONTROL=ignoredups:erasedups
export HISTIGNORE="clear:cd:cd ~:exit:fg*:[ \t]*"
#export PROMPT_COMMAND="history -n; history -w; history -c; history -r; $PROMPT_COMMAND"

# Golang
#if [ -d /usr/local/go ]; then
#	export GOROOT=/usr/local/go
#	export GOBIN=$GOROOT/bin
#	export PATH=$PATH:$GOBIN
#fi
#if [ -d $HOME/Code/go ]; then
#	export GOPATH=$HOME/Code/go
#	export PATH=$PATH:$GOPATH/bin
#fi
GOROOT=/opt/go/go
GOBIN=$GOROOT/bin
GOPATH=/opt/go/path
PATH=$PATH:$GOBIN:$GOPATH/bin

shopt -s histappend

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# Customise input methods
if [ -e ~/.inputrc ]; then
	export INPUTRC=~/.inputrc
fi

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then

    eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='ls --color=auto --format=vertical'
    #alias vdir='ls --color=auto --format=long'
	alias grep='grep --color=auto'
	alias xclip='xclip -selection clipboard'

	# Colorised less
	if [ -f /usr/bin/pygmentize ]; then
		export LESS='-R'
		export LESSOPEN='|pygmentize -f 256 %s'
	fi

fi

# https://github.com/sickill/stderred
_STDERRED_LOCATION=/opt/stderred/build/libstderred.so
if [ -f $_STDERRED_LOCATION ]; then
	export LD_PRELOAD="$_STDERRED_LOCATION${LD_PRELOAD:+:$LD_PRELOAD}"
	#alias stderred="LD_PRELOAD=$_STDERRED_LOCATION\${LD_PRELOAD:+:\$LD_PRELOAD}"

	# Here's an example that will blacklist bash, and all programs with names starting with "test":
	#export STDERRED_BLACKLIST="^(bash|test.*)$"
	export STDERRED_BLACKLIST="^(wget|opusenc|curl|pv|avconv|wkhtmltopdf)$"

	# Custom colours:
	#_bold=$(tput bold || tput md)
    #_red=$(tput setaf 1)
    #export STDERRED_ESC_CODE=`echo -e "$_bold$_red"`
fi
